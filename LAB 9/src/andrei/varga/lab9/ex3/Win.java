package andrei.varga.lab9.ex3;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Win extends JFrame {

    private String path;

    Win() {
        super("Ex3");

        JTextField textField = new JTextField(1000);
        textField.setMaximumSize(new Dimension(1000, 100));
        add(textField);

        JTextArea textArea = new JTextArea();

        JButton button = new JButton("Arata text ");
        button.addActionListener(e -> {
            path = textField.getText();
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
                StringBuilder stringBuilder = new StringBuilder();
                String continut;
                while ((continut = bufferedReader.readLine()) != null) {
                    stringBuilder.append(continut).append('\n');
                }
                textArea.setText(String.valueOf(stringBuilder));
            } catch (FileNotFoundException ex) {
                textArea.setText("Nu exista fisier");
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });


        add(button);
        add(textArea);

        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(500, 500);
        setVisible(true);
    }

}
