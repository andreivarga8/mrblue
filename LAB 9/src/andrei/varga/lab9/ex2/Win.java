package andrei.varga.lab9.ex2;

import javax.swing.*;
import java.awt.*;

public class Win extends JFrame {
    private int apasari = 0;

    public Win() {
        super("Ex2");
        super.setLayout(new FlowLayout());
        super.setDefaultCloseOperation(EXIT_ON_CLOSE);
        super.setSize(250, 100);

        JPanel panel = new JPanel();

        JTextField textField = new JTextField(5);

        JButton button = new JButton("Apasa");
        button.addActionListener(e -> textField.setText(""+ ++apasari));

        panel.setVisible(true);
        add(panel);
        add(textField);
        add(button);
        setVisible(true);
    }
}