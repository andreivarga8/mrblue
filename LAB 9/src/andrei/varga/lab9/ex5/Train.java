package andrei.varga.lab9.ex5;

import java.util.*;

class Train{
    String destination;
    String name;

    public Train(String destinatie, String nume) {
        super();
        this.destination = destinatie;
        this.name = nume;
    }

    String getDestination(){
        return destination;
    }

    public String getName() {
        return name;
    }
}
