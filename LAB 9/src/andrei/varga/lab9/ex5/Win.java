package andrei.varga.lab9.ex5;

import javax.swing.*;
import java.awt.*;

public class Win extends JFrame {

    JLabel stations[] = new JLabel[3];
    JLabel segments[] = new JLabel[9];
    JButton addtrain = new JButton();
    JTextField train = new JTextField();
    JTextField nrseg= new JTextField();

    public Win() {
        setSize(800, 500);
        setLayout(new GridLayout(7, 3));

        stations[0] = new JLabel("Cluj-Napoca");
        stations[1] = new JLabel("Dej");
        stations[2] = new JLabel("Bucuresti");
        for (int i = 0; i < 3; i++) {
            add(stations[i]);
        }

        segments[0] = new JLabel("Seg 1");
        segments[1] = new JLabel("Seg 4");
        segments[2] = new JLabel("Seg 7");
        segments[3] = new JLabel("Seg 2");
        segments[4] = new JLabel("Seg 5");
        segments[5] = new JLabel("Seg 8");
        segments[6] = new JLabel("Seg 3");
        segments[7] = new JLabel("Seg 6");
        segments[8] = new JLabel("Seg 9");

        for (int i = 0; i < 9; i++) {
            add(segments[i]);
        }

        train = new JTextField(10);
        add(train);

        nrseg= new JTextField(5);
        add(nrseg);

        addtrain = new JButton("Add a train");
        addtrain.addActionListener(e -> {
            segments[Integer.parseInt(nrseg.getText())].setText(train.getText());
        });
        add(addtrain);

        pack();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }



}

