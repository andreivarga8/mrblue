package andrei.varga.lab11.ex1;


import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Win extends JFrame {
    JTextField ecran = new JTextField(500);
    public Random temp=new Random();

    public Win() {
        ecran.setMaximumSize(new Dimension(500, 100));
        add(ecran);


        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(500, 100);
        setVisible(true);


    }

    public void updateTextField() {
        int value=temp.nextInt(40);
        ecran.setText(String.valueOf(value)+" degrees Celsius");
       // System.out.println(value);
    }

    public void updateTextField2(){
        ecran.setText("Temp changing...");
    }

}
