package andrei.varga.lab11.ex1;

public class Temp extends Thread {
    public Win win;
    
    public Temp(Win win) {
        this.win = win;

    }

    @Override
    public void run() {

        while (true) {
            win.updateTextField();
            try {
                currentThread().sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            win.updateTextField2();
            try {
                currentThread().sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

