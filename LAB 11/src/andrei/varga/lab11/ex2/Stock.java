package andrei.varga.lab11.ex2;

import javax.sound.midi.Soundbank;
import java.util.ArrayList;

public class Stock {
    private Monitor monitor;
    private ArrayList<Product> products;

    public Stock(Monitor monitor) {
        this.monitor = monitor;
        products = new ArrayList<>();
    }

    public void seeProducts() {
        System.out.println("The products are: ");
        for (Product i : products) {
            System.out.println(i.getName());
        }
    }

    public void addProduct(Product product) {
        products.add(product);
        monitor.added(product);
    }

    public void removeProduct(String name) {
        this.products.remove(getProduct(name));
        monitor.remove(name);
    }

    public Product getProduct(String name) {
        for (Product i : products) {
            if (i.getName().equals(name)) {
                return i;
            }
        }
        return null;
    }
}
