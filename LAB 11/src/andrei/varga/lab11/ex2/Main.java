package andrei.varga.lab11.ex2;

public class Main {
    public static void main(String[] args) {
        Monitor monitor = new Monitor();
        Stock stock = new Stock(monitor);

        stock.addProduct(new Product("AJ1", 20, 160, monitor));
        stock.addProduct(new Product("Air Force 1", 100, 40, monitor));
        stock.addProduct(new Product("Nike Hyperdunk", 95, 30, monitor));
        stock.addProduct(new Product("Nike Vapormax", 120, 20, monitor));
        stock.addProduct(new Product("Gucci sandals", 2000, 5, monitor));
        stock.seeProducts();
        stock.getProduct("Gucci sandals").setQuantity(150);
        stock.removeProduct("Nike Vapormax");
        stock.seeProducts();
    }
}
