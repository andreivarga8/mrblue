package andrei.varga.lab11.ex2;

public class Product {
    private String name;
    private double price;
    private double quantity;
    private Monitor monitor;

    public Product(String name, double price, int quantity, Monitor monitor) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.monitor = monitor;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
        monitor.update(name, quantity);
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
