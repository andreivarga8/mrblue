package andrei.varga.lab11.ex2;

public class Monitor {
    public void update(String name, int number) {
        System.out.println("item " + name + " has " + number + " left");
    }

    public void remove(String name) {
        System.out.println("The item " + name + " has been removed");
    }

    public void added(Product product) {
        System.out.println("The item " + product.getName() + " was added to the stock");
    }
}
