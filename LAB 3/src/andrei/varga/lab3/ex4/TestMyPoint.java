package andrei.varga.lab3.ex4;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint a = new MyPoint(10,14);
        MyPoint b = new MyPoint(0, 1);
        System.out.println(a.toString());
        System.out.println(b.toString());
        System.out.println("distance from a to b: " + a.distance(b));
    }
}
