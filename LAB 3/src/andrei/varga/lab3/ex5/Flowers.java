package andrei.varga.lab3.ex5;

public class Flowers {
    int petal;
    static int flowers = 0;
    Flowers(int p){
        flowers++;
        petal=p;
        System.out.println("New flower has been created!");
    }

    public static void main(String[] args) {
        Flowers f1 = new Flowers(4);
        Flowers f2 = new Flowers(6);
        Flowers f3 = new Flowers(6);
        Flowers f4 = new Flowers(6);
        Flowers f5 = new Flowers(6);
        Flowers f6 = new Flowers(6);
        System.out.println("We have " + f1.flowers + " flowers");
    }
}
