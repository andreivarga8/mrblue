package andrei.varga.lab3.ex2;

public class Circle {
    private double radious=1.0;
    private String color="red";

    public Circle(double x, String y){
        this.radious=x;
        this.color=y;
    }
    public Circle(){
    this(1.67,"blue");
    }
    public double getRadious() {
        return radious;
    }

    public String getColor() {
        return color;
    }
}


