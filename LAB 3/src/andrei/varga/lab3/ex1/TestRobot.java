package andrei.varga.lab3.ex1;

public class TestRobot {
    public static void main(String[] args) {
        Robot r1 = new Robot();
        Robot r2 = new Robot();
        Robot r3 = new Robot();
        r1.change(10);
        r2.change(2);
        r3.change(0);
        System.out.println(r1.toString());
        System.out.println(r2.toString());
        System.out.println(r3.toString());
    }
}
