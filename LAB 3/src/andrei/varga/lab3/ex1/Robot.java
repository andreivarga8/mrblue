package andrei.varga.lab3.ex1;

public class Robot {
    int x; //pozitia robotului

    public Robot() {
        x = 1;
    }

    public void change(int k) {
        if (k >= 1)
            x = k + x;
    }

    public String toString() {
        return "the position is " + x;
    }
}


