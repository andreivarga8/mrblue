package andrei.varga.lab5.ex3;

abstract public class Sensor {
    private String location;

    public String getLocation() {
        return location;
    }

    public abstract int readValue();
}
