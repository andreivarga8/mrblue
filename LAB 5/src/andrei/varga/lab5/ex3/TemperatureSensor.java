package andrei.varga.lab5.ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor {
    @Override
    public int readValue() {
        Random r1 = new Random();
        int x = r1.nextInt(100);
        return x;
    }
}
