package andrei.varga.lab5.ex3;

import andrei.varga.lab5.ex3.*;

public class Controller {
    public void control() {
        int i = 0;
        TemperatureSensor temperatureSensor = new TemperatureSensor();
        LightSensor lightSensor = new LightSensor();
        while (i < 20) {
            System.out.println("temperature=" + temperatureSensor.readValue());
            System.out.println("light=" + lightSensor.readValue());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
    }
}
