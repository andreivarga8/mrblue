package andrei.varga.lab5.ex4;

import andrei.varga.lab5.ex3.*;

public class Controller1 {
    TemperatureSensor tempSensor = new TemperatureSensor();
    LightSensor lightSensor = new LightSensor();
    private static Controller1 controller;

    private Controller1() {
    }

    public static Controller1 getController() {
        if (controller == null)
            controller = new Controller1();
        return controller;
    }

    public void control() {
        int i = 0;
        TemperatureSensor temperatureSensor = new TemperatureSensor();
        LightSensor lightSensor = new LightSensor();
        while (i < 20) {
            System.out.println("temperature=" + temperatureSensor.readValue());
            System.out.println("light=" + lightSensor.readValue());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
    }
}
