package andrei.varga.lab5.ex1;

public class Main {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[6];

        shapes[0] = new Circle(2, true, "red");
        shapes[1] = new Circle(3, false, "blue");
        shapes[2] = new Circle(5, true, "black");
        shapes[3] = new Rectangle(7, 2);
        shapes[4] = new Rectangle(2, 4);
        shapes[5] = new Square(5);

        int i;
        for (i = 0; i < 6; i++) {
            System.out.println("for " + shapes[i].toString() + " we have area=" + shapes[i].getArea() + " and permieter=" + shapes[i].getPerimeter() + "\n");
        }
    }
}