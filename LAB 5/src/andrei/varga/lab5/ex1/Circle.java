package andrei.varga.lab5.ex1;

public class Circle extends Shape {
    protected double radius ;
    public Circle(double radius){
        this.radius=radius;
    }
    public Circle(){
        this.radius=0;
    }
    public Circle(double radius, boolean filled, String color ){
        super(color,filled);
        this.radius=radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    double getArea() {
        return 3.14*3.14*radius;
    }

    @Override
    double getPerimeter() {
        return 2*3.14*radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }
}
