    package andrei.varga.lab4.ex6;

public class Circle extends Shape {
    private double rad = 1.0;

    public Circle() {

    }

    public Circle(double rad) {
        this.rad = rad;
    }

    public Circle(double rad, String color, boolean filled) {
        super(color, filled);
        this.rad = rad;
    }

    public double getRad() {
        return rad;
    }

    public void setRad(double rad) {
        this.rad = rad;
    }

    public double getArea() {
        return 3.14 * rad * rad;
    }

    public double getPerimeter() {
        return 2 * 3.14 * rad;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "rad=" + rad +
                "perimeter=" + this.getPerimeter() +
                "area=" + this.getArea() +
                '}';
    }
}
