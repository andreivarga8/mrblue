package andrei.varga.lab4.ex2;

public class Author {
    private String name;
    private String mail;
    private String gender;

    public Author(String name, String mail,String gender){
        this.name=name;
        this.mail=mail;
        this.gender=gender;
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return "Author{" +
                "name='" + name + '\'' +
                ", mail='" + mail + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
}
