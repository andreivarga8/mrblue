package andrei.varga.lab4.ex5;

public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder() {
    }

    public Cylinder(double rad) {
        super(rad);
    }

    public Cylinder(double rad, double height) {
        super(rad);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return getArea() * height;
    }

    @Override

    public double getArea() {
        double Ab = super.getArea();
        double Al = 3.14 * super.getRad() * height;

        return Ab + Al;
    }


}
