package andrei.varga.lab4.ex5;

public class Circle {
    private double rad = 1.0;
    private String color = "red";

    public Circle() {
        rad = 0;
        color = "";
    }

    public Circle(double rad) {
        this.rad = rad;
    }

    public double getRad() {
        return rad;
    }

    public double getArea() {
        return 3.14 * this.rad * this.rad;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + rad +
                ", color='" + color + '\'' +
                '}';
    }
}