package andrei.varga.lab4.ex4;

import andrei.varga.lab4.ex2.Author;

import java.util.Arrays;

public class MainBook {
    public static void main(String[] args) {
        Author a1= new Author("Mircea","mircea@yahoo.com","m");
        Author a2= new Author("Vlad","vlad@yahoo.com","m");
        Author[] authors={a1,a2};
        Book b1 = new Book("Ceata",authors,100);

        //System.out.println(Arrays.toString(b1.getAuthors()));
        b1.printAuthors();
    }
}
