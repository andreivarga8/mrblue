package andrei.varga.lab4.ex3;

import andrei.varga.lab4.ex2.Author;

public class MainBook {
    public static void main(String[] args) {
        Author a1= new Author("matei","mate@yahoo.com","m");

        Book b1= new Book("Miau", a1 ,10);
        System.out.println(b1.getName());
        System.out.println(b1.getAuthor());
        b1.setPrice(100);
        System.out.println(b1.getPrice());
        b1.setQtyInStock(1000);
        System.out.println(b1.getQtyInStock());
        System.out.println(b1.toString());
    }
}
