package andrei.varga.lab4.ex1;

import java.nio.charset.CoderResult;

public class MainCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        System.out.println(c1.getRad());
        System.out.println(c1.getArea());
        Circle c2= new Circle(2);
        System.out.println(c2.getRad());
        System.out.println(c2.getArea());
    }
}
