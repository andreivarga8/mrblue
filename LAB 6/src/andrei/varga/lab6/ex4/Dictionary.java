package andrei.varga.lab6.ex4;

import java.util.HashMap;

public class Dictionary {

    HashMap<Word, Definition> dictionary = new HashMap<>();


    public void addWord(Word w, Definition d) {
        if (dictionary.containsKey(w)) {
            System.out.println("Cuvantul este deja existent");
            return;
        }
        dictionary.put(w, d);
    }

    public String getDefinition(Word w) {
        for (Word i : dictionary.keySet()) {
            if(i.getName().equals(w.getName())){
               return dictionary.get(i).toString();
            }
        }
        return null;
    }


    public void getAllWords() {
        System.out.println("The words are:");
        for (Word i : dictionary.keySet()) {
            System.out.println(i.getName());
        }
        System.out.println('\n');
    }

    public void getAllDefinitions() {
        System.out.println("the definitions are: ");
        for (Word i : dictionary.keySet()) {
            System.out.println(dictionary.get(i).toString());
        }
        System.out.println('\n');
    }

    public HashMap<Word, Definition> getHashMap() {
        return (this.dictionary);
    }
}
