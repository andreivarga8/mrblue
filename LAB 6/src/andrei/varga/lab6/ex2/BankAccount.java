package andrei.varga.lab6.ex2;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.balance = balance;
        this.owner = owner;
    }

    public void witdraw(double amount) {
        if ((amount > 0) && (balance - amount) > 0) this.balance = this.balance - amount;
        else System.out.println("Transaction cannot be made.");
    }

    public void deposit(double amount) {
        if (amount > 0) this.balance = this.balance + amount;
        else System.out.println("Transaction cannot be made! ");
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof andrei.varga.lab6.ex1.BankAccount) {
            andrei.varga.lab6.ex1.BankAccount bankAccount = (andrei.varga.lab6.ex1.BankAccount) object;
            return balance == bankAccount.balance;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }
}
