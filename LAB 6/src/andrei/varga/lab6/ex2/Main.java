package andrei.varga.lab6.ex2;

import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();

        bank.addAccount("Varga", 5000);
        bank.addAccount("Popica", 1200);
        bank.addAccount("Dioane", 499);
        bank.addAccount("Cristi", 10000);

        bank.printAccounts();
        System.out.println('\n');
        bank.printAccounts(500, 15000);
        System.out.println('\n');

        //aranjez dupa balance
        Collections.sort(bank.accounts, new Comp());
        for (BankAccount i : bank.accounts)
            System.out.println(i.toString());
        System.out.println('\n');

        //aranjez in ordine alfabetica ownerii de conturi
        Collections.sort(bank.accounts, new Comp2());
        for (BankAccount i : bank.accounts)
            System.out.println(i.toString());


    }
}
