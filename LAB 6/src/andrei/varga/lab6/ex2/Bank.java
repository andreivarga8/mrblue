package andrei.varga.lab6.ex2;

import java.util.ArrayList;
import java.util.Collections;

public class Bank {

    ArrayList<BankAccount> accounts = new ArrayList<>();

    public void addAccount(String owner, double balance) {
        BankAccount bankAccount = new BankAccount(owner, balance);
        accounts.add(bankAccount);
    }

    public void printAccounts() {
        Collections.sort(accounts, new Comp());

        for (BankAccount i : accounts)
            System.out.println(i.toString());
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount i : accounts) {
            if (i.getBalance() >= minBalance && i.getBalance() <= maxBalance) {
                System.out.println(i.toString());
            }
        }
    }

    public BankAccount getAccount(String owner) {
        for (BankAccount i : accounts)
            if (i.getOwner() == owner) return i;
        return null;
    }


}
