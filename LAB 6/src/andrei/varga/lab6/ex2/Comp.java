package andrei.varga.lab6.ex2;

import java.util.Comparator;

public class Comp implements Comparator<BankAccount> {
    //compar dupa balance
    public int compare(BankAccount x1, BankAccount x2) {
        double dif = x1.getBalance() - x2.getBalance();
        if (dif == 0) return 0;
        if (dif > 0) return 1;
        return -1;
    }
}

class Comp2 implements Comparator<BankAccount> {
    //compar dupa owner
    public int compare(BankAccount x1, BankAccount x2) {
        return (int) x1.getOwner().compareTo(x2.getOwner());

    }
}