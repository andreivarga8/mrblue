package andrei.varga.lab6.ex3;

import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {

            Bank bank = new Bank();

            bank.addAccounts("Varga", 5000);
            bank.addAccounts("Popica", 1200);
            bank.addAccounts("Dioane", 499);
            bank.addAccounts("Cristi", 10000);

            bank.printAccounts();
            System.out.println('\n');
            bank.printAccounts(500, 15000);
            System.out.println('\n');
        }

        //fiind treeset deja sunt ordonate in functie de ceea ce am dat cand am creeat TreeSetul ( new Comp2())

}
