package andrei.varga.lab6.ex1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<BankAccount> list1 = new ArrayList<>();
        BankAccount bankAccount1, bankAccount2, bankAccount3;
        bankAccount1 = new BankAccount("Varga", 1000);
        bankAccount2 = new BankAccount("Popica", 1000);
        bankAccount3 = new BankAccount("Dio", 999);

        list1.add(bankAccount1);
        list1.add(bankAccount2);
        list1.add(bankAccount3);

        Iterator<BankAccount> it = list1.iterator();

        while (it.hasNext()) {
            String name = (String) it.next().getOwner();
            System.out.println(name);
        }
        for (int i = 0; i < list1.size(); i++) {
            if (i == list1.size() - 1) break;
            System.out.println(
                    "comparison between "
                            + i
                            + " and "
                            + (int) (i + 1)
                            + " -> "
                            + list1.get(i).equals(list1.get(i + 1))
            );
            System.out.println(list1.get(i).hashCode());
        }
    }
}
