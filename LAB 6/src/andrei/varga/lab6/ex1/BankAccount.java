package andrei.varga.lab6.ex1;

import java.util.Objects;

public class BankAccount {
    public double balance;
    private String owner;

    public BankAccount(String owner, double balance) {
        this.balance = balance;
        this.owner = owner;
    }

    public void withdraw(double amount) {
        if ((amount > 0) && (balance - amount) > 0) this.balance = this.balance - amount;
        else System.out.println("Transaction cannot be made.");
    }

    public void deposit(double amount) {
        if (amount > 0) this.balance = this.balance + amount;
        else System.out.println("Transaction cannot be made! ");
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof BankAccount) {
            BankAccount bankAccount = (BankAccount) object;
            return balance == bankAccount.balance;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }
}
