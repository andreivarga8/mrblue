package andrei.varga.lab2.ex5;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int i, j, x;
        int aux;
        int[] a = new int[100];
        Random rand = new Random();
        for (i = 0; i <= 9; i++) {
            x = rand.nextInt(100);
            a[i] = x;
        }
        System.out.println("Numerele initiale");
        for (i = 0; i <= 9; i++) {
            System.out.println(a[i] + "");
        }

        //Sortarea
        for (i = 0; i <= 9; i++) {
            for (j = 0 ; j <= 9-i-1; j++) {
                if (a[j] > a[j + 1]) {
                    aux = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = aux;
                }
            }
        }

        System.out.println("Numerele sortate");
        for (j = 0; j <= 9; j++) {
            System.out.println(a[j] + "");
        }

    }
}

