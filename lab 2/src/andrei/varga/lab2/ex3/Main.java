package andrei.varga.lab2.ex3;

import java.util.Scanner;

class GFG {

    static boolean isPrime(int n) {
        if (n <= 1)
            return false;
        for (int i = 2; i < n; i++)
            if (n % i == 0)
                return false;
        return true;
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int j;
        for (j = a; j <= b; j++) {
            if(GFG.isPrime(j))
                System.out.println(j + " Prime");
        }
    }
}

