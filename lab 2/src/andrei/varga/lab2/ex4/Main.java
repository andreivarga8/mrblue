package andrei.varga.lab2.ex4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] v = new int[100];
        int i, a, max = 0;
        for (i = 0; i < n; i++) {
            a = in.nextInt();
            v[i] = a;
        }
        for (i = 0; i < n; i++) {
            if (v[i] > max) {
                max = v[i];
            }
        }
        System.out.println(max);
    }
}

