package andrei.varga.lab10.ex4;

import java.util.Random;

public class Robot extends Thread {

    Caracter rob = new Caracter();
    Point p = new Point();
    BoundingBox boundingBox;
    Random random = new Random();
    boolean run = true;

    Robot() {
        super();
        this.boundingBox = new BoundingBox(p, Utils.characterSize);
        rob.setBounds(random.nextInt(Utils.winSize - Utils.characterSize),
                      random.nextInt(Utils.winSize - Utils.characterSize),
                      Utils.characterSize,
                      Utils.characterSize);
        rob.setVisible(true);

    }

    public void run() {

        while (run) {
            int x = random.nextInt(Utils.winSize - Utils.characterSize);
            int y = random.nextInt(Utils.winSize - Utils.characterSize);
            p.setXY(x, y);
            System.out.println("" + x + "," + y);
            rob.setLocation(p.x, p.y);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    BoundingBox getBoundingBox() {
        return boundingBox;
    }

    boolean testCollision(Robot rob) {
        return this.getBoundingBox().testCollision(rob.getBoundingBox());
    }

    Caracter getCharacter() {
        return rob;
    }

    void stopRobot() {
        rob.setVisible(false);
        run = false;
    }
}