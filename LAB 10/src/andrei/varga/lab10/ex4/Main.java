package andrei.varga.lab10.ex4;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Window w = new Window();
        ArrayList<Robot> robot = new ArrayList<>();
        int nbrobots= 10;
        for (int i = 0; i < nbrobots; ++i) {
            robot.add(new Robot());
            w.add(robot.get(i).getCharacter());
            robot.get(i).start();
        }
        w.setVisible(true);
        while (nbrobots > 1) {
            for (int i = 0; i < nbrobots; ++i) {
                for (int j = i + 1; j < nbrobots; ++j) {
                    if (robot.get(i).testCollision(robot.get(j))) {
                        robot.get(i).stopRobot();
                        robot.get(j).stopRobot();
                        robot.remove(j);
                        robot.remove(i);
                        nbrobots= nbrobots-2;
                        Thread.sleep(500);
                    }
                }
            }
            System.out.println(nbrobots);
            w.repaint();
            Thread.sleep(1000);
        }
    }
}

