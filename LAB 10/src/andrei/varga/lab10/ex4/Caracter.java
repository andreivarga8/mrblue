package andrei.varga.lab10.ex4;

import javax.swing.*;
import java.awt.*;


public class Caracter extends JComponent {

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.GREEN);
        g.fillRect(0, 0, Utils.characterSize, Utils.characterSize);
    }

}