package andrei.varga.lab10.ex6;

import javax.swing.*;

public class Win extends JFrame {
    private Object ecran;
    JLabel afis = new JLabel();

    Win(Object ecran) {
        this.ecran = ecran;
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(300, 200);


        afis.setBounds(110, 10, 280, 20);

        JButton startStop = new JButton("Start/Stop");
        startStop.setBounds(10, 40, 250, 20);
        startStop.addActionListener(e -> {
            synchronized (ecran){
                ecran.notify();
            }
        });
        add(afis);
        add(startStop);
        setVisible(true);
    }

    public void updateTime(String time) {
        afis.setText(time);
    }
}
