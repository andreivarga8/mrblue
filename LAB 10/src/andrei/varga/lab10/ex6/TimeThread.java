package andrei.varga.lab10.ex6;

public class TimeThread extends Thread {
    public Win win;
    public Object ecran;

    public TimeThread(Object ecran, Win win) {
        this.ecran = ecran;
        this.win = win;
    }

    @Override
    public void run() {
        synchronized (ecran) {
            while (true) {
                try {
                    ecran.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                win.updateTime("Time passing... ");
                long t1 = System.currentTimeMillis();
                try {
                    ecran.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                long t2 = System.currentTimeMillis();
                win.updateTime("Time: " + (t2 - t1));
            }
        }
    }
}
