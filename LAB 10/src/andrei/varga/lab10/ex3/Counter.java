package andrei.varga.lab10.ex3;

public class Counter implements Runnable {
    public static Integer nb = 0;

    public Counter() {
    }

    static void print() {
        System.out.println(Thread.currentThread().getName() + ": " + nb);
    }

    @Override
    public void run() {
        synchronized (this) {
            for(int i=0;i<100;i++) {
                nb++;
                print();
            }
        }
    }

    public static void main(String[] args) {
        Counter counter = new Counter();
        Thread thread1 = new Thread(counter);
        Thread thread2 = new Thread(counter);
        thread1.start();
        thread2.start();


    }
}
