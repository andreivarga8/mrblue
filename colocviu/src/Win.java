import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class Win extends JFrame {

    private JTextField field1 = new JTextField(50);
    ;
    private JTextField field2 = new JTextField(50);
    private JButton button = new JButton("Arata numarul de caractere");
    private JPanel panel = new JPanel();

    public Win() {
        this.panel.setLayout(new BorderLayout());

        JPanel center = new JPanel();
        center.setLayout(new GridLayout(2, 1));
        center.add(field1);
        center.add(field2);

        this.panel.add(center, BorderLayout.CENTER);
        this.panel.add(button, BorderLayout.SOUTH);

        this.setContentPane(this.panel);
        this.pack();
        this.setSize(600, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public String getField1() {
        return this.field1.getText();
    }


    public void setField2(String text) {
        this.field2.setText(text);
    }

    public void addButtonListener(ActionListener actionListener) {
        this.button.addActionListener(actionListener);
    }
}
