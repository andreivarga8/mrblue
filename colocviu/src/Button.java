import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Button {

    private Win win;

    public Button(Win win) {
        this.win = win;
        this.win.addButtonListener(new ButtonListener());
    }

    class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            win.setField2(Integer.toString(FileReader.NoOfChars(win.getField1())));
        }
    }
}
