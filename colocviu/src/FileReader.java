import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileReader {

    public static int NoOfChars(String file) {
        File input = new File(file);
        int nbChars = 0;

        try {
            FileInputStream fis = new FileInputStream(file);
            char current;
            while (fis.available() > 0) {
                current = (char) fis.read();
                nbChars++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return nbChars;
    }
}
