package andrei.varga.lab7.ex2;

import java.io.*;
import java.util.Scanner;

public class CountLetter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        BufferedReader input = null;
        try {
            input = new BufferedReader(new FileReader("src\\andrei\\varga\\lab7\\ex2\\fisier.txt"));
            int charCount = 0;
            int character;
            System.out.println("Insert a character: ");
            char c = scanner.next().charAt(0);
            while ((character = input.read()) != -1) {
                if ((char) character == c) charCount++;
            }
            System.out.println("Numarul de aparitii ale caracterului " + c + " este " + charCount);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
