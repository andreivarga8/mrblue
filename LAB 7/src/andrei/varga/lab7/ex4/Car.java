package andrei.varga.lab7.ex4;

import java.io.*;
import java.util.Arrays;
import java.util.Objects;

public class Car implements Serializable {
    private String model;
    private double price;

    public Car(String model, double price) {
        this.model = model;
        this.price = price;
    }

    public void SaveCar() {
        try {
            FileOutputStream fileout = new FileOutputStream("src\\andrei\\varga\\lab7\\ex4\\Cars\\" + model + ".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileout);
            out.writeObject(this);
            out.close();
            fileout.close();
            System.out.println("Car saved");

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    public static void displayCars() {
        File dir = new File("src\\andrei\\varga\\lab7\\ex4\\Cars");
        for(File f : dir.listFiles()){
            System.out.println(f.getName());
        }
    }

    public static Car readModel(String model) {
        try {
            FileInputStream fileIn = new FileInputStream("src\\andrei\\varga\\lab7\\ex4\\Cars\\" + model + ".ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Car car = (Car) in.readObject();
            in.close();
            fileIn.close();
            return car;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                '}';
    }

}
