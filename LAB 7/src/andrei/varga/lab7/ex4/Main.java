package andrei.varga.lab7.ex4;


import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int contiuna = 1;
        Car car = null;
        while (contiuna != 0) {
            System.out.println("1.Create car");
            System.out.println("2.Save car");
            System.out.println("3.View cars");
            System.out.println("4.View a specific car");
            System.out.println("5.Exit");
            int alegere = scanner.nextInt();
            switch (alegere) {
                case 1:
                    car = createCar();
                    break;
                case 2:
                    saveCar(car);
                    break;
                case 3:
                    viewAllCars();
                    break;
                case 4:
                    oneCar();
                    break;
                case 5:
                    contiuna=0;
                    break;

            }
        }
    }


    private static Car createCar() {
        System.out.println("Car model");
        String model = scanner.next();
        System.out.println("Price");
        int price = scanner.nextInt();
        return new Car(model, price);
    }

    private static void saveCar(Car car) {
        if (car != null)
            car.SaveCar();
        else
            System.out.println("Create ca car and try again");
    }

    private static void viewAllCars() {
        Car.displayCars();
    }

    private static void oneCar() {
        System.out.println("Enter car name");
        String model = scanner.next();
        Car c = Car.readModel(model);
        if (c != null)
            System.out.println(c.toString());
        else
            System.out.println("Couldn't find car");
    }
}
